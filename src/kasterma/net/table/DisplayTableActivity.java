package kasterma.net.table;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.ViewFlipper;

public class DisplayTableActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Log.v("DTA", "started");
        
        Table T = new Table((TableLayout) findViewById(R.id.mainTable), this);  // TODO: don't pass this, pass context???
        T.build();
        
        Log.v("DTA", "table build");
    }
    
    public void toTable (View view) {
    	Log.v("DTA", "flip to table");
		ViewFlipper flipper = (ViewFlipper) findViewById(R.id.mainFlipper);
		flipper.setInAnimation(FlipperAnimations.inFromRightAnimation());
		flipper.setOutAnimation(FlipperAnimations.outToLeftAnimation());
		flipper.showNext();    	
    }
}