package kasterma.net.table;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

public class RotatedTextView extends TextView {
	final boolean topDown;

	public RotatedTextView(Context context){
		super(context);
		final int gravity = getGravity();
		topDown = true;
	}

		   @Override
		   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		      super.onMeasure(heightMeasureSpec, widthMeasureSpec);
		      setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
		   }

		   @Override
		   protected void onDraw(Canvas canvas){
		      TextPaint textPaint = getPaint(); 
		      textPaint.setColor(getCurrentTextColor());
		      textPaint.drawableState = getDrawableState();

		      canvas.save();

		      if(topDown){
		         canvas.translate(getWidth(), 0);
		         canvas.rotate(90);
		      }else {
		         canvas.translate(0, getHeight());
		         canvas.rotate(-90);
		      }


		      canvas.translate(getCompoundPaddingLeft(), getExtendedPaddingTop());

		      getLayout().draw(canvas);
		      canvas.restore();
		  }
		}
