package kasterma.net.table;

import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Table {
	TableLayout TL;
	DisplayTableActivity DLA;
	
	Table(TableLayout TLa, DisplayTableActivity DLAa) {
		TL = TLa;
		DLA = DLAa;
	}
	void build() {
		Log.v("Table", "Building");
		TableRow TRheaders = new TableRow(DLA);
		for (String item:TableData.Headers) {
			RotatedTextView TV = new RotatedTextView(DLA);
			TV.setText(item);
		    TRheaders.addView(TV);
		}
		TL.addView(TRheaders);
		Log.v("Table", "Headers written");
		
		for (String [] row : TableData.Rows) {
			TableRow TR = new TableRow(DLA);
			for (String item:row) {
				TextView TV = new TextView(DLA);
				TV.setText(item);
			    TR.addView(TV);
			}
			TL.addView(TR);
		}
		Log.v("Table", "Rows written");
	}
}
