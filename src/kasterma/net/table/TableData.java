package kasterma.net.table;

public class TableData {
	static String[] Headers = {"Team", "Gespeeld", "Punten", "Gewonnen",
			"Gelijk", "Verloren", "Goals", "Tegengoals"};

	static String[][] Rows = {
		{"Brugge", "8", "18", "5", "3", "0", "17", "8"},
		{"Anderlecht", "8", "17", "5", "2", "1", "14", "8"},
		{"AA Gent", "8", "16", "5", "1", "2", "16", "11"},
		{"Cercle Brugge", "8", "15", "4", "3", "1", "10", "7"},
		{"Genk", "8", "13", "3", "4", "1", "15", "10"},
		{"Mons", "8", "12", "3", "3", "2", "18", "13"},
		{"Standard", "8", "12", "3", "3", "2", "10", "10"},
		{"Kortrijk", "8", "11", "3", "2", "3", "10", "7"},
		{"OH Leuven", "8", "10", "3", "1", "4", "9", "10"},
		{"Mechelen", "8", "10", "3", "1", "4", "9", "15"},
		{"Beerschot", "8", "9", "2", "3", "3", "13", "13"},
		{"Zulte Waregem", "8", "9", "2", "3", "3", "8", "12"},
		{"Lokeren", "8", "8", "2", "2", "4", "7", "8"},
		{"Lierse", "8", "6", "0", "6", "2", "4", "6"},
		{"Westerlo", "8", "5", "1", "2", "5", "6", "15"},
		{"Sint-Truidense", "8", "1", "0", "1", "7", "12", "25"},
	};
}
